import java.util.LinkedList;
import java.util.Iterator;

/**
 * This class represents a cache tailored to the BTreeIO operations.
 * 
 * Cache size determined by the user from command line input
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */

public class Cache<T extends Comparable<T>> implements Iterable<T> {
	int capacity;
	LinkedList<T> data;

	public Cache(int CacheSize) {
		data = new LinkedList<T>();
		capacity = CacheSize;
	}

	/**
	 * Removes the object from the cache
	 * 
	 * @param obj
	 *            is the object to be removed
	 */
	public T remove(T obj) {
		T out = null;
		T curr = null;
		for (Iterator<T> i = data.iterator(); i.hasNext();) {
			curr = i.next();
			if (curr.compareTo(obj) == 0) {
				out = curr;
				i.remove();
				break;
			}
		}
		return out;
	}

	public boolean check(T checkCache) {
		return data.contains(checkCache);
	}

	/**
	 * Adds an object to the front of the cache.
	 * 
	 * @param toAdd
	 *            is the object to be added
	 */
	public T add(T addObj) {
		data.push(addObj);
		if (data.size() > capacity)
			return data.removeLast();
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		return data.iterator();
	}
}
