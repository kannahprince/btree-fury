import java.util.ArrayList;

/**
 * A node in the BTree Structure.
 * 
 * A BTreeNode maintains a list of key values, a list of children, and an index
 * to its parent. The node is also aware of whether it is a root or leaf node.
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */
public class BTreeNode implements Comparable<BTreeNode> {
	public ArrayList<TreeObject> keys; // ArrayList of keys stored in node as TreeObjects
	public ArrayList<Integer> children; // List of indexes for each child node
	public int index; // Index of node in Btree
	public int parent; // Index of the parent node.
	public int maxKeyCount; // Max keys a node can hold
	public boolean root; // Flag for root node
	public boolean leaf; // Flag for leaf node
	public int degree; // Degree of the BTree used for calculations for BTree

	/**
	 * BTreeNode constructor - Creates a new BTreeNode to be inserted into the
	 * BTree.
	 * 
	 * @param degree
	 *            the degree of the BTree which contains this node.
	 * @param root
	 *            a boolean defining whether this node is the root of the tree.
	 * @param leaf
	 *            a boolean defining whether this node is a leaf node.
	 * @param index
	 *            the index of this node in the BTree.
	 */
	public BTreeNode(int degree, boolean root, boolean leaf, int index) {
		this.degree = degree;
		this.index = index;
		this.root = root;
		this.leaf = leaf;
		if (this.root)
			parent = -1;

		maxKeyCount = 2 * degree - 1;
		keys = new ArrayList<TreeObject>(maxKeyCount);
		children = new ArrayList<Integer>(maxKeyCount + 1);
	}

	/**
	 * Check if the node is full. A full node has 2*degree-1 keys.
	 * 
	 * @return true if the node is full, false otherwise.
	 */
	public boolean isFull() {
		return keys.size() == maxKeyCount;
	}

	@Override
	public int compareTo(BTreeNode tObj) {
		return index - tObj.index;
	}
}
