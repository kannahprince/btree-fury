import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

/**
 * Search a Btree file for various dna sequences using the data from a query
 * file which contains a list of DNA strings.
 * 
 * Prints a list of frequencies of sequences from the query
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */

public class GeneBankSearch {
	private static BTree btree;
	private static int degree;
	private static int sequenceLength;
	private static int debugLevel;
	private static int cacheSize;
	private static String btreeFile;
	private static String queryFile;

	/**
	 * This will read a BTree node from the BTree file and start searching for
	 * the DNA sequence from the query file.
	 */
	private static void searchBtreeFile() {
		String line;
		BufferedReader reader;

		try {
			btree = new BTree(new File(btreeFile), degree, sequenceLength,
					cacheSize, debugLevel);
			reader = new BufferedReader(new FileReader(queryFile));

			if (debugLevel == 0) {
				System.out.println("Genebank search of " + btreeFile);
				System.out.println("Using strings from " + queryFile);
				System.out.println("<Freq>\t<Query String>");
				System.out.println("------\t--------------");
			}

			while ((line = reader.readLine()) != null) {
				line = line.toUpperCase();

				if (debugLevel == 0)
					System.out.println(btree.Search(line) + "\t" + line);
				else
					btree.Search(line);
			}
		} catch (IllegalArgumentException e) {
			System.err
			.println("Incongruent degree or sequenceLength from what was read from file.");
			e.printStackTrace();
			System.exit(1);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Prints correct command line arguments for GeneBankSearch
	 */
	private static void printUsage() {
		System.out
		.println("Usage for GeneBankSearch: \n"
				+ "java GeneBankSearch <btree file> <query file> <cache size>[<debug level>]\n\n"
				+ "<btree file>              created BTree file\n"
				+ "<query file>              file containing DNA strings to search\n"
				+ "<cache size>              size of cache you wish to use > 0"
				+ "[<debug level>]           0 for diagnostic, help, and status messages.");
	}

	public static void main(String[] args) {
		/* If the argument count is not 3 or 4 exit the program */
		if (args.length < 3 || args.length > 4) {
			printUsage();
			System.exit(1);
		}

		String[] arg = args[0].split("\\.");
		degree = Integer.parseInt(arg[4]);
		sequenceLength = Integer.parseInt(arg[3]);
		cacheSize = Integer.parseInt(args[2]);
		btreeFile = args[0];
		queryFile = args[1];
		if (args.length == 4)
			debugLevel = Integer.parseInt(args[3]);
		else
			debugLevel = -1;

		searchBtreeFile();
	}
}
