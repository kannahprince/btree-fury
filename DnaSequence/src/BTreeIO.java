import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * This class represents a BTreeIO.
 * 
 * Maintains the writing and reading from file.
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */

public class BTreeIO {
	private RandomAccessFile ioFile;
	private Cache<BTreeNode> cache;
	private int degree, length;

	public BTreeIO(String fname, int degree, int length, int cache_size,
			int debug) throws FileNotFoundException {
		ioFile = new RandomAccessFile(fname, "rw");
		cache = new Cache<BTreeNode>(cache_size);
		this.degree = degree;
		this.length = length;
	}

	/**
	 * Attempts to read a BTreeNode from cache or disk if it cannot be found in
	 * the cache.
	 * 
	 * @param treeIndex
	 *            the index of the node that is being read in.
	 * @param degree
	 *            the degree of the BTree.
	 * @return
	 * 
	 * @throws IOException
	 *             if there is an error while reading the file.
	 */
	public BTreeNode readBTreeNode(int treeIndex, int degree)
			throws IOException {
		BTreeNode newNode = new BTreeNode(1, false, false, treeIndex);
		newNode = cache.remove(newNode);
		if (newNode != null) {
			newNode.children = newNode.children;
			newNode.degree = newNode.degree;
			newNode.index = newNode.index;
			newNode.keys = newNode.keys;
			newNode.leaf = newNode.leaf;
			newNode.maxKeyCount = newNode.maxKeyCount;
			newNode.parent = newNode.parent;
			newNode.root = newNode.root;
			cache.add(newNode);
			return newNode;
		}

		newNode = new BTreeNode(1, false, false, treeIndex);

		int keycount;
		newNode.degree = degree;

		newNode.index = treeIndex;
		newNode.root = false;
		newNode.maxKeyCount = 2 * degree - 1;

		ByteBuffer buffer = ByteBuffer.allocate(4 + 4 + 4 + newNode.maxKeyCount
				* (8 + 4) + (newNode.maxKeyCount + 1) * (4));

		ioFile.seek(getNodeOffset(treeIndex));
		ioFile.read(buffer.array());

		/* Read meta data */
		newNode.parent = buffer.getInt();

		if (buffer.getInt() == 1) {
			newNode.leaf = true;
		} else {
			newNode.leaf = false;
		}

		keycount = buffer.getInt();

		/* Read keys */
		newNode.keys = new ArrayList<TreeObject>(keycount);

		for (int k = 0; k < newNode.maxKeyCount; k++) {
			/* Read in each key and store it in the list. */
			if (k < keycount) {
				newNode.keys.add(k, new TreeObject(buffer));
			}
			/* Skip past each empty key. */
			else {
				buffer.getLong();
				buffer.getInt();
			}
		}
		newNode.children = new ArrayList<Integer>(keycount + 1);

		if (!newNode.leaf) {
			for (int c = 0; c < newNode.maxKeyCount + 1; c++) {
				/* Read in each child and store it in the list. */
				if (c < keycount + 1) {
					newNode.children.add(c, buffer.getInt());
				}
				/* Skip past each empty child. */
				else {
					buffer.getInt();
				}
			}
		}
		BTreeNode returnNode = newNode;
		newNode = cache.add(newNode);
		if (newNode != null)
			writeToFile(newNode, false);
		return returnNode;
	}

	/**
	 * Returns the byte offset of a given BTreeNode in the file given its index.
	 * 
	 * @param index
	 *            the index of the node relative to the BTree.
	 * @return the byte offset of the node in the file.
	 */
	protected int getNodeOffset(int index) {
		return BTree.SIZE_OF_META_DATA + (sizeOfBTreeNode(degree) * index);
	}

	/**
	 * Attempts to write the BTreeNode into the given RandomAccessFile. root
	 * node it is only written when writeRootFlag is true at EOF
	 * 
	 * @param bTreeIO
	 *            the RandomAccessFile to write to.
	 * @param writeRootFlag
	 *            Write root node if true.
	 * 
	 * @throws IOException
	 *             if an error occurs while writing to the file.
	 */
	public void writeToFile(BTreeNode node, boolean writeRootFlag)
			throws IOException {
		if (node.root && !writeRootFlag)
			return;
		ByteBuffer buffer = ByteBuffer.allocate(4 + 4 + 4 + node.maxKeyCount
				* (8 + 4) + (node.maxKeyCount + 1) * (4));

		ioFile.seek(getNodeOffset(node.index));

		/* Write meta data */
		buffer.putInt(node.parent);
		buffer.putInt(node.leaf ? 1 : 0);
		buffer.putInt(node.keys.size());

		/* Write keys */
		for (int k = 0; k < node.maxKeyCount; k++) {
			if (k < node.keys.size()) {
				buffer.putLong(node.keys.get(k).sequence);
				buffer.putInt(node.keys.get(k).getFrequency());
			}
			/* Pad remaining keys space with empty TreeObjects. */
			else {
				buffer.putLong(-1);
				buffer.putInt(-1);
			}
		}

		/* Write children */
		for (int c = 0; c < node.maxKeyCount + 1; c++) {
			if (c < node.children.size()) {
				buffer.putInt(node.children.get(c));
			} else {
				buffer.putInt(-1);
			}
		}
		ioFile.write(buffer.array());
	}

	/**
	 * Writes the meta data to the file.
	 * 
	 * @throws IOException
	 *             if an error occurs while writing to disk.
	 */
	protected void writeMetaData(int degree, int rootIndex, int sequenceLength)
			throws IOException {
		ioFile.seek(0L);
		ioFile.writeInt(degree);
		ioFile.writeInt(rootIndex);
		ioFile.writeInt(sequenceLength);
	}

	/**
	 * Writes all nodes in memory to disk and writes the meta data at EOF
	 * 
	 * @throws IOException
	 *             if an error occurs while writing to disk.
	 */
	public void flushTree(BTreeNode current, BTreeNode next, BTreeNode root)
			throws IOException {
		int rootIndex = root.index;
		assert (root != null);
		if (current != null)
			writeToFile(current, true);
		if (next != null)
			writeToFile(next, true);
		writeToFile(root, true);
		writeMetaData(degree, rootIndex, length);
	}

	/**
	 * Calculates the size of a BTreeNode for the given degree
	 * according the BTree definition.
	 * 
	 * @param degree
	 *            the degree of the BTree.
	 * 
	 * @return the maximum size of a BTreeNode for a BTree of the given degree.
	 */
	public int sizeOfBTreeNode(int degree) {
		int maxKeyCount = 2 * degree - 1;
		int metaDataSize = (BTree.SIZE_OF_INT * 2) + BTree.SIZE_OF_BOOLEAN;
		int keysSize = TreeObject.SIZE * (maxKeyCount);
		int childrenSize = BTree.SIZE_OF_INT * (maxKeyCount + 1);
		int BTreeNodeSize = metaDataSize + keysSize + childrenSize;
		return BTreeNodeSize;
	}

	public int readMetaDegree() throws IOException {
		ioFile.seek(0L);
		return ioFile.readInt();
	}

	public int readMetaRootIndex() throws IOException {
		ioFile.seek(4L);
		return ioFile.readInt();
	}

	public int readMetaSequenceLength() throws IOException {
		ioFile.seek(8L);
		return ioFile.readInt();
	}
}