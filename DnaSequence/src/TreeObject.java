import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;

/**
 * Stores an object(DNA subsequence) in a tree structure.
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */
public class TreeObject implements Comparable<TreeObject> {
	/**
	 * Size of an instantiated TreeObject in bytes.
	 */
	public static final int SIZE = 12; // size of long + size of int
	public final long sequence;
	private int frequency;

	/**
	 * Default constructor for a TreeObject. Create an instance of an empty
	 * TreeObject.
	 */
	public TreeObject() {
		sequence = 0;
		frequency = 0;
	}

	/**
	 * Attempts to read a TreeObject from the given RandomAccessFile
	 * 
	 * @param io
	 *            - RandomAccessFile to read from
	 * @throws IOException
	 */
	public TreeObject(ByteBuffer buf) throws IOException {
		sequence = buf.getLong();
		frequency = buf.getInt();
	}

	/**
	 * Create a new TreeObject from the given DNA subsequence.
	 * 
	 * @param s
	 *            the DNA subsequence to create the TreeObject from.
	 */
	public TreeObject(String seq) {
		sequence = convertStringToBinary(seq);
		frequency = 1;
	}

	/**
	 * Retrieve this object formatted as a String. Returns a string containing
	 * the binary format of the key and the frequency
	 * 
	 * @return String output of object
	 */
	public String toString() {
		return String.format(
				"%d %s",
				frequency,
				convertBinaryToString(sequence,
						GeneBankCreateBTree.sequenceLength));
	}

	/**
	 * Retrieve the frequency count for this DNA subsequence.
	 * 
	 * @return the frequency as an int.
	 */
	public int getFrequency() {
		return frequency;
	}

	/**
	 * Increase the frequency count by 1.
	 */
	public void incrementFrequency() {
		frequency++;
	}

	/**
	 * Compares this object to another TreeObject using long values
	 * 
	 * @param b
	 *            the TreeObject to compare this object to.
	 * 
	 * @return 0 if both objects are equal
	 * 
	 */
	public int compareTo(TreeObject b) {
		if (this.sequence > b.sequence)
			return 1;
		else if (this.sequence < b.sequence)
			return -1;
		else
			return 0;
	}

	/**
	 * Converts a DNA subsequence string into a long. Each character of the
	 * string is converted to a 2-bit binary number.
	 * 
	 * @param sequence
	 *            the String to convert.
	 * @return the long representation of the subsequence.
	 */
	public static long convertStringToBinary(String sequence) {
		sequence = sequence.toUpperCase();
		StringBuilder output = new StringBuilder();
		Character c;
		for (int i = 0; i < sequence.length(); i++) {
			c = sequence.charAt(i);
			switch (c) {
			case 'A':
				output.append("00");
				break;
			case 'C':
				output.append("01");
				break;
			case 'G':
				output.append("10");
				break;
			case 'T':
				output.append("11");
				break;
			}
		}
		return parseLong(output.toString());
	}

	private static long parseLong(String s) {
		return new BigInteger(s, 2).longValue();
	}

	/**
	 * Converts a DNA subsequence long back into a string for printing.
	 * 
	 * @param seq
	 *            - long representation of a sequence
	 * @param sequenceLength
	 *            # of char in the sequence
	 */
	public static String convertBinaryToString(long key, int sequenceLength) {
		StringBuffer output = new StringBuffer(sequenceLength);
		output.setLength(sequenceLength);
		for (int i = 0; i < sequenceLength; i++) {
			int value = (int) (key % 4);
			key = key >> 2;
		switch (value) {
		case 0:
			output.setCharAt(sequenceLength - i - 1, 'A');
			break;
		case 1:
			output.setCharAt(sequenceLength - i - 1, 'C');
			break;
		case 2:
			output.setCharAt(sequenceLength - i - 1, 'G');
			break;
		case 3:
			output.setCharAt(sequenceLength - i - 1, 'T');
			break;
		}
		}
		return output.toString();
	}
}
