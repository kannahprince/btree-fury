import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Creates a BTree out of a GeneBank File.
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */

public class GeneBankCreateBTree {
	private static BTree bTreeIO;
	private static int debugLevel = -1;
	private static String fname;
	private static int degree, cacheSize;
	public static int sequenceLength;

	public static void main(String[] args) throws FileNotFoundException {
		if (args.length < 4 || args.length > 5) {
			System.err.println("Invalid number of arguments.\n");
			printUsage();
			System.exit(-1);
		}

		try {
			degree = Integer.parseInt(args[0]);
			fname = args[1];
			sequenceLength = Integer.parseInt(args[2]);
			cacheSize = Integer.parseInt(args[3]);
		} catch (NumberFormatException e) {
			printUsage();
			// e.printStackTrace();
			System.exit(1);
		}

		if (sequenceLength > 31) {
			System.err.println("Invalid sequence length.");
			printUsage();
			System.exit(-1);
		}
		if (degree < 2) {
			System.err.println("Invalid degree, caculating optimal degree.");
			// printUsage();
			// System.exit(-1);
			degree = 0;
		}

		if (args.length > 4)
			debugLevel = Integer.parseInt(args[4]);

		if (debugLevel == 1) {
			System.err.println("Dumping BTree to Dump File!");
		}

		parseFile(degree, sequenceLength, cacheSize);
		if (debugLevel == 1)
			debugMode(Integer.parseInt(args[4]));

		if (debugLevel == 1) {
			System.err.println("Bree Successfully Dumped!");
		}
	}

	/**
	 * Parses the given file and inserts each sequence into a BTree
	 * 
	 * @param fname
	 *            The location of the file
	 * @param degree
	 *            The degree of the BTree
	 * @param sequenceLength
	 *            The length of the sequences the BTree will be used for.
	 */
	private static void parseFile(int degree, int sequenceLength, int cacheSize) {
		try {
			FileParser parser = new FileParser(sequenceLength, new File(fname));
			int i = 0;
			if (parser.hasNext(i)) {
				bTreeIO = new BTree(degree, sequenceLength, fname + ".btree."
						+ sequenceLength + "." + degree, parser.next(i),
						cacheSize, debugLevel);
				while ((parser.hasNext(i)) && (i < parser.getStrLength())) {
					if (parser.next(i).toUpperCase().contains("N")) {
						i++;
						continue;
					} else {
						bTreeIO.BTreeInsert(parser.next(i));
						i++;
					}
				}
				bTreeIO.flushTree();
			}

			else {
				System.err.println("No Sequences found.");
				System.exit(-1);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find file: " + fname);
			// e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			// e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Writes the bTree to a dump file if the debug mode is 1
	 * 
	 * @param debug
	 *            The debug level.
	 */
	private static void debugMode(int debug) {
		try {
			bTreeIO.inOrderTraversal(new File("dump.txt"), fname);
		} catch (IOException e) {
			System.out
			.println("Unable to open file for writing, check permissions");
			e.printStackTrace();
		}
	}

	public static void printUsage() {
		System.err
		.println("USAGE:\n"
				+ "\t$ java GeneBankCreateBtree <degree> <gbk file> <sequence length> <cache size> [<debug level>]\n");
		System.err
		.println("\t\t<degree>: int - degree of BTree: value >= 2 (default level = 2)");
		System.err
		.println("\t\t<gbk file>: File path to Gene Bank file to parse.");
		System.err
		.println("\t\t<sequence length>: int - Length of the dna chain [1 - 31].");
		System.err.println("\t\t<cache size>: int - value >= 0.");
		System.err
		.println("\t\t<debug level>: Determines output data [0 or 1].");
	}
}
