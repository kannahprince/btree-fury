import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Stack;

/**
 * This class represents a BTree.
 * 
 * Each node also has a number of children equal to 1 more than the number of
 * keys in the node, except for the leaf nodes which do not have children.
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */

public class BTree {
	/* Size of primitive types for storage on ioFile. */
	public static final int SIZE_OF_INT = 4;
	public static final int SIZE_OF_LONG = 8;
	public static final int SIZE_OF_BOOLEAN = 4;
	public static final int SIZE_OF_META_DATA = SIZE_OF_INT * 3;
	
	private BTreeNode root, current, next; // Nodes in memory; accessible at any time
	private int degree; // Used for determining # of keys and children allowed
	private int sequenceLength; // Length of DNA sequence (1 to 31 allowed)
	private int nodeCount; // Current number of nodes in the BTree
	private int debugLevel; // Optional user input to determine output
	private BTreeIO ioFile; // IO File to store nodes not in cache

	/**
	 * Constructor for creating a new BTree.
	 * 
	 * Instantiates a new BTree object with one node at index 0 which stores a
	 * single key(sequence).
	 * 
	 * @param degree
	 *            the degree of this BTree.
	 * @param length
	 *            the DNA sequence length stored in this BTree.
	 * @param path
	 *            the file path to the new BTree node.
	 * @param sequence
	 *            the initial key value to insert into the root node.
	 * 
	 * @throws FileNotFoundException
	 *             if the file indicated by path could not be created.
	 */
	public BTree(int degree, int length, String fname, String sequence, int cache_size, int debug) throws FileNotFoundException {
		debugLevel = debug;
		ioFile = new BTreeIO(fname, degree, length, cache_size, debug);

		if (degree == 0) {
			this.degree = calculateOptimalDegree();
		} else {
			this.degree = degree;
		}

		sequenceLength = length;

		File f = new File(fname);
		f.delete(); // Remove existing file before creating the new BTree.

		TreeObject key = new TreeObject(sequence);
		root = new BTreeNode(this.degree, true, true, 0);
		root.keys.add(0, key);
		nodeCount = 1;

		printDebug("Created BTree with Root Node: " + root.index
				+ " Sequence: " + sequence);
	}

	/**
	 * Constructor for an existing BTree that uses the given file as it's data
	 * source. BTree will read and search from the given files.
	 * 
	 * @param f
	 *            the File to be read from.
	 * @param degree
	 *            the degree of the the BTree stored in f.
	 * @param sequenceLength
	 *            the sequence length that was used to create the BTree stored
	 *            in f.
	 * 
	 * @throws FileNotFoundException
	 *             if the file f does not exist.
	 * @throws IllegalArgumentException
	 *             if the values for degree or sequenceLength do not match what
	 *             is stored on disk.
	 */
	public BTree(File f, int degree, int sequenceLength, int cache_size,
			int debug) throws FileNotFoundException, IllegalArgumentException,
			IOException {
		debugLevel = debug;

		// cache = new Cache<BTreeNode>(cache_size);
		ioFile = new BTreeIO(f.getName(), degree, sequenceLength, cache_size,
				debug);

		if (degree == 0)
			degree = calculateOptimalDegree();

		// read in the meta data.
		this.degree = ioFile.readMetaDegree();
		int rootIndex = ioFile.readMetaRootIndex();
		this.sequenceLength = ioFile.readMetaSequenceLength();

		// Verify that the degree and sequenceLength match what was read.
		if (degree != this.degree && sequenceLength != this.sequenceLength)
			throw new IllegalArgumentException();

		root = ioFile.readBTreeNode(rootIndex, degree);
		root.root = true;
	}

	private void printDebug(String msg) {
		if (debugLevel == 0) {
			System.err.println(msg);
		}
	}

	/**
	 * Calculates the optimal degree for a BTree. This method assumes a disk
	 * block is 4096 bytes. It tests increasing values of degree until it finds
	 * a degree that would produce a BTree node with a size greater than 4096.
	 * It then returns the previously tested degree since it is the greatest
	 * degree that will produce a node with size less than or equal to 4096.
	 * 
	 * @return the optimal degree for a BTree.
	 */
	private int calculateOptimalDegree() {
		int optimalDegree = 0;
		int nodeSize = 0;

		while (nodeSize <= 4096) {
			nodeSize = ioFile.sizeOfBTreeNode(++optimalDegree);
		}
		
		optimalDegree--;
		printDebug("Optimal degree of " + optimalDegree
				+ " has been calculated.");
		return optimalDegree;
	}

	/**
	 * Splits a child of parent and inserts its median key into parent.
	 * 
	 * @param parent
	 *            the index of the node whose child will be split.
	 * @param toSplit
	 *            the index of the node that will be split.
	 * @param i
	 *            the index of toSplit in parent's children array.
	 * 
	 * @throws IllegalStateException
	 *             if this node is full or if toSplit is not full.
	 * @throws IOException
	 *             if there is an error while writing to disk.
	 */
	public void splitChild(BTreeNode parent, BTreeNode toSplit, int i)
			throws IllegalStateException, IOException {
		if (parent.isFull() || !toSplit.isFull())
			throw new IllegalStateException();

		printDebug("Child Split with parent Node: " + parent.index
				+ " Split Indexes: " + toSplit.index + " and " + i);

		BTreeNode newChild = new BTreeNode(degree, false, toSplit.leaf,
				nodeCount);
		nodeCount++;

		printDebug("Created New child Node: " + newChild.index
				+ " Parent Node: " + parent.index);

		// Move the largest t-1 keys from toSplit into newChild.
		for (int j = toSplit.keys.size() - 1; j > degree - 1; j--) {
			newChild.keys.add(0, toSplit.keys.remove(j));
		}

		// Then move the corresponding t children of those keys from toSplit
		// into newChild.
		if (!toSplit.leaf) {
			for (int j = toSplit.children.size() - 1; j >= degree; j--) {
				newChild.children.add(0, toSplit.children.remove(j));
			}
		}

		// Insert the node newChild as a child of parent.
		parent.children.add(i + 1, newChild.index);

		// Set parent as the parent of newChild
		newChild.parent = parent.index;

		// Move the median key of toSplit up to parent
		parent.keys.add(i, toSplit.keys.remove(degree - 1));

		// Write all three nodes to disk.
		ioFile.writeToFile(parent, false);
		ioFile.writeToFile(toSplit, false);
		ioFile.writeToFile(newChild, false);
	}

	/**
	 * Inserts the given DNA sequence into the BTree.
	 * 
	 * @param sequence
	 *            the DNA subsequence to insert.
	 * 
	 * @throws IOException
	 *             if an attempt to read a BTreeNode from disk fails.
	 */
	public void BTreeInsert(String sequence) throws IOException {
		if (root.isFull()) {
			// Save the current root node to a temp variable.
			next = root;

			// Create a new root node with the old root as its first child.
			root = new BTreeNode(degree, true, false, nodeCount++);
			root.children.add(0, next.index);
			next.parent = root.index;
			next.root = false;

			printDebug("Created New Root Node: " + root.index);

			// Split the old root (next) so that the new root has a key and two
			// children.
			splitChild(root, next, 0);

			// Now try to insert the sequence into the non-full root node.
			InsertNonFull(new TreeObject(sequence));
		} else {
			InsertNonFull(new TreeObject(sequence));
		}
	}

	/**
	 * Inserts the given key into the BTree into a node which is not full.
	 * 
	 * Searches for the correct node to insert the key into. Full nodes are
	 * split as they are encountered.
	 * 
	 * @param key
	 *            the TreeObject which contains the key to insert.
	 * 
	 * @throws IOException
	 *             if an attempt to read a BTreeNode from disk fails.
	 */
	private void InsertNonFull(TreeObject key) throws IOException {
		boolean searching = true;
		current = root;

		while (searching) {
			int i = current.keys.size() - 1;

			if (current.leaf) {
				// Look for the correct place to insert the new key by
				// searching in descending order. If the key we are
				// trying to insert is found during this search we
				// just increase its frequency and return.
				while (i >= 0 && key.compareTo(current.keys.get(i)) <= 0) {
					if (key.compareTo(current.keys.get(i)) == 0) {
						current.keys.get(i).incrementFrequency();
						ioFile.writeToFile(current, false);
						printDebug("Sequence : "
								+ TreeObject.convertBinaryToString(
										key.sequence, sequenceLength)
										+ " Node: " + current.index + " Frequency++");
						return;
					}

					i--;
				}

				// Insert the new key into the correct position.
				current.keys.add(i + 1, key);
				printDebug("Sequence : "
						+ TreeObject.convertBinaryToString(key.sequence,
								sequenceLength) + " Node: " + current.index
								+ " Inserted");
				ioFile.writeToFile(current, false);
				searching = false;
			} else {
				// Look for the first key that is greater than the key we are
				// inserting by searching in descending order. If the key we 
				// are trying to insert is found during this search we just
				// increase its frequency count and return.
				while (i >= 0 && key.compareTo(current.keys.get(i)) <= 0) {
					if (key.compareTo(current.keys.get(i)) == 0) {
						current.keys.get(i).incrementFrequency();
						ioFile.writeToFile(current, false);
						printDebug("Sequence : "
								+ TreeObject.convertBinaryToString(
										key.sequence, sequenceLength)
										+ " Node: " + current.index + " Frequency++");
						return;
					}
					i--;
				}
				i++;

				// read in the child node
				next = ioFile.readBTreeNode(current.children.get(i), degree);
				// System.out.println("ioFile returned: "+next);
				if (next.isFull()) {
					splitChild(current, next, i);

					// Before descending into the child we need to make sure
					// that the median key that was just moved up isn't the key
					// we are trying to insert. If it is we need to increase its
					// frequency and return.
					if (key.compareTo(current.keys.get(i)) == 0) {
						current.keys.get(i).incrementFrequency();
						ioFile.writeToFile(current, false);
						printDebug("Sequence : "
								+ TreeObject.convertBinaryToString(
										key.sequence, sequenceLength)
										+ " Node: " + current.index + " Frequency++");

						return;
					}

					// We need to know which child to descend to. If it is child
					// i this comparison will be < 0. If it is the new child i+1
					// this comparison will be > 0.
					else if (key.compareTo(current.keys.get(i)) > 0) {
						next = ioFile.readBTreeNode(
								current.children.get(i + 1), degree);
					}
				}
				current = next;
			}
		}
	}

	/**
	 * Given a DNA sequence, returns the number of times that sequence has been
	 * inserted.
	 * 
	 * @param sequence
	 *            The DNA subsequence to search for.
	 * 
	 * @return the frequency of the given DNA subsequence in the BTree.
	 * 
	 * @throws IOException
	 *             if a BTreeNode cannot be read.
	 */
	public int Search(String sequence) throws IOException {
		return searchNodeGetFreq(root, new TreeObject(sequence));
	}

	/**
	 * Searches the tree for a given key beginning with the specified node.
	 * 
	 * @param node
	 *            the node to start the search from.
	 * @param searchKey
	 *            the key we are searching for.
	 * 
	 * @return the frequency of the given key. 0 if the key was not found.
	 * 
	 * @throws IOException
	 *             if a BTreeNode cannot be read.
	 * 
	 */
	private int searchNodeGetFreq(BTreeNode node, TreeObject searchKey)
			throws IOException {
		current = node;
		boolean searching = true;

		while (searching) {
			int i = 0;

			while (i < current.keys.size()
					&& searchKey.compareTo(current.keys.get(i)) > 0) {
				i++;
			}

			if (i < current.keys.size()
					&& searchKey.compareTo(current.keys.get(i)) == 0) {
				return current.keys.get(i).getFrequency();
			}

			if (current.leaf) {
				return 0; // case where sequence is not found
			} else {
				current = ioFile.readBTreeNode(current.children.get(i), degree);
			}
		}

		return -1;
	}

	/**
	 * Dumps each element in the BTree into dump.txt file using an inorder
	 * traversal.
	 * 
	 * @param dumpFile
	 *            The path to dump the elements.
	 */
	public void inOrderTraversal(File dumpFile, String fname)
			throws IOException {
		FileOutputStream outFileStream = new FileOutputStream(dumpFile);
		PrintWriter outStream = new PrintWriter(outFileStream);
		Stack<Integer> nodes = new Stack<Integer>();
		Stack<Integer> children = new Stack<Integer>();

		outStream.println("Contents of a BTree from " + fname);
		outStream.println("Degree of BTree = " + degree);
		outStream.println("DNA Sequence length = " + sequenceLength);
		outStream.println("The key values are displayed in ascending order.");
		outStream.println();
		outStream.println("<Freq>\t<DNA Sequence>");
		outStream.println("-------------------------");

		boolean traversingFlag = true;
		current = root;
		int child = 0;

		while (traversingFlag) {
			// Traverse to all children inorder
			if (child == current.children.size() && !current.leaf) {
				if (nodes.empty() && children.empty()) {
					traversingFlag = false;
					continue;
				} else {
					// Ascend to parent
					current = ioFile.readBTreeNode(nodes.pop(), degree);
					child = children.pop();

					// if this isn't the last child in parent print the next key
					if (child < current.keys.size()) {
						dumpWriteTreeObject(current.keys.get(child), outStream);
					}
					child++;
					continue;
				}
			}

			// Print all of the keys and ascend back to the parent.
			if (current.leaf) {
				for (int k = 0; k < current.keys.size(); k++) {
					dumpWriteTreeObject(current.keys.get(k), outStream);
				}

				if (current == root)
					break; // single node in tree

				// ascend back to parent
				current = ioFile.readBTreeNode(nodes.pop(), degree);
				child = children.pop();

				// Not last child in parent therefore print next key
				if (child < current.keys.size()) {
					dumpWriteTreeObject(current.keys.get(child), outStream);
				}

				child++;
			} else // Not last child not leaf therefore descend to the next
				// child.
			{
				// push node and child index to stacks
				nodes.push(current.index);
				children.push(child);

				// Read child node
				current = ioFile.readBTreeNode(current.children.get(child),
						degree);
				child = 0;
			}
		}
		outStream.close();
	}

	/**
	 * Prints the sequence and frequency count of the given tree to the File
	 * referenced by out.
	 * 
	 * @param key
	 *            the key to print.
	 * @param outStream
	 *            the file handle to write the data to.
	 * 
	 * @throws IOException
	 *             if an error occurs while writing to the file.
	 */
	private void dumpWriteTreeObject(TreeObject tObj, PrintWriter outStream)
			throws IOException {
		outStream.write(tObj.getFrequency()
				+ "\t "
				+ TreeObject.convertBinaryToString(tObj.sequence,
						sequenceLength));
		outStream.println();
	}

	public void flushTree() throws IOException {
		ioFile.flushTree(current, next, root);

	}
}
// ====================END OF BTREE CLASS=====================\\
