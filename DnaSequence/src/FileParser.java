import java.io.File;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class represents the file parser.
 * 
 * Parses a genebank file (.gbk) to gain all the needed data for the dna
 * sequences
 * 
 * @author Stephen Porter
 * @author Mike Perez
 * @author Curtis Ransom
 */
public class FileParser {
	int sequenceLength;
	File gbkFile = null;
	String tempLine;
	String parseLine = null;
	String startParse = "ORIGIN";
	String endParse = "//";
	boolean parseFlag = false;
	BufferedReader br = null;

	/* =============================Constructor=============================== */
	public FileParser(int sequenceLength, File file) throws IOException {
		this.sequenceLength = sequenceLength;
		gbkFile = file;
		parseFile();
	}

	/* ========================= Read and Parse File ========================= */
	public String parseFile() throws IOException {
		try {
			FileReader fr = new FileReader(gbkFile);
			br = new BufferedReader(fr);
			StringBuffer stringBuffer = new StringBuffer();

			while ((tempLine = br.readLine()) != null) {
				String[] strarr = tempLine.split(" ");

				for (String str : strarr) {
					if (str.equalsIgnoreCase(startParse)) {
						// System.out.println(startParse + " exists in " +
						// tempLine); //debugging purposes
						tempLine = br.readLine();
						parseFlag = true;
					}
					if (str.equalsIgnoreCase(endParse)) {
						// System.out.println(endParse + " exists in " +
						// tempLine); //debugging purposes
						parseFlag = false;
					}
				}
				tempLine = tempLine.replaceAll("\\s", "");
				tempLine = tempLine.replaceAll("[^a-zA-Z]", "");
				// System.out.println("Temp line for buffer: " + tempLine);
				// //debugging purposes

				if (parseFlag == true) {
					stringBuffer.append(tempLine);
				}
			}
			fr.close();
			parseLine = stringBuffer.toString();
		} catch (FileNotFoundException e) {
			System.err.println("Cannot find file: " + gbkFile);
			// e.printStackTrace();
			System.exit(1);
		}
		return parseLine;
	}

	public Boolean hasNext(int i) {
		boolean result = false;
		try {
			if (((parseLine.substring(i, i + sequenceLength)) != null)
					&& (i + sequenceLength < parseLine.length())) {
				result = true;
			}
		} catch (StringIndexOutOfBoundsException e) {
			// Catch no sequences
		}
		return result;
	}

	public int getStrLength() {
		return parseLine.length();
	}

	public String next(int i) {
		String sequence = parseLine.substring(i, i + sequenceLength);
		return sequence;
	}
}